<?php 
	session_start();
	$session["stream"] = "off";
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Secure Home</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
   
        
        <style>
        	img{
        		width: 70%;
        		height: auto;
        	}
        </style>
        
        
         <script>
			<?php
				if($_SESSION["stream"] == "off"){
			?>
			
				$(document).ready(function(){
				        $("#webcamimage").hide();
				        $("#show").show();
				        $("#hide").hide();
				        $("#pnostream").show();		    
				});
			<?php
				}
			?>					
		</script>

        <script>
        
       
			$(document).ready(function(){
			    $("#hide").click(function(){
			        $("#webcamimage").hide();
			    });
			    $("#hide").click(function(){
			        $("#show").show();
			    });
			    $("#hide").click(function(){
			        $("#hide").hide();
			    });		
			    $("#hide").click(function(){
			        $("#pnostream").show();
			    });				    
			});

			
			    
			  $(document).ready(function(){      
			    $("#show").click(function(){
			        $("#webcamimage").show();
			    });
			    $("#show").click(function(){
			        $("#hide").show();
			    });
			    $("#show").click(function(){
			        $("#show").hide();
			    });
			    $("#show").click(function(){
			        $("#pnostream").hide();
			    });	
			});			
			
		</script>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-light  bg-light  fixed-top" id="mainNav">
    <a class="navbar-brand" href="dashbord.php">Secure Home</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        
		
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Example Pages">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
            <i class="fa fa-wrench"></i>
            <span class="nav-link-text">Control panel</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseExamplePages">
            <li>
              <a class="fa fa-fw fa-dashboard" data-placement="right" href="dashbord.php">Dashboard</a>
            </li>
            <li>
              <a class="fa fa-users" href="register.php">Manage acces</a>
            </li>
          </ul>
        </li>
		
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
          <a class="nav-link" href="charts.php">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">Charts</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
          <a class="nav-link" href="tables.php">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">Log Data</span>
          </a>
        </li>
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Camera">
          <a class="nav-link" href="Camera.php">
            <i class="fa fa-camera"></i>
            <span class="nav-link-text">Camera</span>
          </a>
        </li>
     
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>
  </nav>
  
  
  
  
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Your Camera</li>
      </ol>
 
	
	<div id="maindiv">
	  	<br>
  	
  		<div align="center">
  			<button id="hide">hide</button>
  			<button id="show">Show</button>
  			
  		</div>
  		
  		<br>
  		
  		<div align="center" id="oopInfo">
  			
  			<h2 id="pnostream">er is momenteel geen stream.</h2>
  		</div>
  		<br>
  		<div id="lesdiv">
  			<div id="webcamimage" align="center">   	
     		<img src= "http://RpiMilleJelle:8081/?action=stream" width="800" height="460"/>   
      		</div>
      	
  			
  		</div>
  		

  	</div>
	
	

    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Milleville Jelle/Thijs Buyse Website 2018</small>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>
    <script src="js/sb-admin-charts.min.js"></script>
  </div>
</body>

</html>
