#include "cmsis_os.h"                   // CMSIS RTOS header file
#include "GUI.h"
#include "Dialog.h"
#include <stdio.h>
#include <string.h>
#include "stm32f7xx_hal.h"

#define ID_TEXT_0        (GUI_ID_USER + 0x07)

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart6;
extern WM_HWIN CreateLoraWan(void);
//extern uint8_t buff_rec[4];
char str[200];
//int fgetc(FILE *f);
//int fputc(int c, FILE *f);
//extern int  GUI_VNC_X_StartServer(int, int);
 
#ifdef _RTE_
#include "RTE_Components.h"             // Component selection
#endif
 
/*----------------------------------------------------------------------------
        GUIThread: GUI Thread for Single-Task Execution Model
 *---------------------------------------------------------------------------*/
// /* I2C Driver */
//#define _I2C_Driver_(n) Driver_I2C##n
//#define I2C_Driver_(n) _I2C_Driver_(n)
//extern ARM_DRIVER_I2C I2C_Driver_(TSC_I2C_PORT);
//#define ptrI2C (&I2C_Driver_(TSC_I2C_PORT))

//static int32_t TC74_Read (uint8_t reg, uint8_t *val) {
//	uint8_t data[1];

//	data[0] = reg;
//	ptrI2C->MasterTransmit(TC74_I2C_ADDR, data, 1, true);
//	while (ptrI2C->GetStatus().busy);
//	if (ptrI2C->GetDataCount() != 1) return -1;
//	ptrI2C->MasterReceive (TC74_I2C_ADDR, val, 1, false);
//	while (ptrI2C->GetStatus().busy);
//	if (ptrI2C->GetDataCount() != 1) return -1;

//	return 0;
//}

 
 
 
 
 
void GUIThread (void const *argument);              // thread function
osThreadId tid_GUIThread;                           // thread id
osThreadDef (GUIThread, osPriorityIdle, 1, 2048);   // thread object
 
int Init_GUIThread (void) {
  tid_GUIThread = osThreadCreate (osThread(GUIThread), NULL);
  if (!tid_GUIThread) return(-1);
  
  return(0);
}
 
void GUIThread (void const *argument) {
 	WM_HWIN hWin;
  GUI_Init();                     /* Initialize the Graphics Component */
  //GUI_VNC_X_StartServer(0,0);
  hWin = CreateLoraWan();
  while (1) {
    	WM_HWIN hItem;
    /* All GUI related activities might only be called from here */
 
	#ifdef RTE_Graphics_Touchscreen   /* Graphics Input Device Touchscreen enabled */
			GUI_TOUCH_Exec();             /* Execute Touchscreen support */
	#endif
			GUI_Exec();                   /* Execute all GUI jobs ... Return 0 if nothing was done. */
			GUI_X_ExecIdle();             /* Nothing left to do for the moment ... Idle processing */
			osDelay(100);
  }
}

//int fputc(int c, FILE *f) {
//	return (HAL_UART_Transmit(&huart1, (uint8_t *)&c,1,HAL_MAX_DELAY));
//}

//int fgetc(FILE *f) {
//	char ch;
//	HAL_UART_Receive(&huart1,(uint8_t*)&ch,1,HAL_MAX_DELAY);
//	return (ch);
//}