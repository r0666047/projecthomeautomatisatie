
/*
 * Auto generated Run-Time-Environment Component Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'helloworld_inout' 
 * Target:  'Target 1' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H


/*
 * Define the Device Header File: 
 */
#define CMSIS_device_header "stm32f0xx.h"

#define RTE_Compiler_IO_STDIN           /* Compiler I/O: STDIN */
          #define RTE_Compiler_IO_STDIN_User      /* Compiler I/O: STDIN User */
#define RTE_Compiler_IO_STDOUT          /* Compiler I/O: STDOUT */
          #define RTE_Compiler_IO_STDOUT_User     /* Compiler I/O: STDOUT User */
#define RTE_DEVICE_CUBE_MX_HAL
#define RTE_DEVICE_FRAMEWORK_CUBE_MX
#define RTE_Drivers_USART1_Async        /* Driver USART1 */
        #define RTE_Drivers_USART2_Async        /* Driver USART2 */
        #define RTE_Drivers_USART3_Async        /* Driver USART3 */
        #define RTE_Drivers_USART4_Async        /* Driver USART4 */
        #define RTE_Drivers_USART5_Async        /* Driver USART5 */
        #define RTE_Drivers_USART6_Async        /* Driver USART6 */
        #define RTE_Drivers_USART7_Async        /* Driver USART7 */
        #define RTE_Drivers_USART8_Async        /* Driver USART8 */

#endif /* RTE_COMPONENTS_H */
